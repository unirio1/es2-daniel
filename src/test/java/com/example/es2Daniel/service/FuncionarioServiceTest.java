package com.example.es2Daniel.service;

import com.example.es2Daniel.exception.FuncionarioNaoEncontradoException;
import com.example.es2Daniel.models.Funcionario;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.annotation.DirtiesContext;

import static org.junit.jupiter.api.Assertions.*;

@SpringBootTest
@DirtiesContext(classMode = DirtiesContext.ClassMode.AFTER_EACH_TEST_METHOD)
public class FuncionarioServiceTest {

    @Autowired
    private FuncionarioService funcionarioService;

    @AfterEach
    public void clear(){
        funcionarioService = null;
    }

    @Test
    public void testCadastrarFuncionario() {
        Funcionario funcionario =  new Funcionario(1, "dan@gmail.com", "Dan", 18,
                "Funcionario", "123123123", "123123", "123123");

        assertNotNull(funcionarioService.cadastrarFuncionario(funcionario));
    }

    @Test
    public void testListarFuncionarios() {
        Funcionario funcionario =  new Funcionario(1, "dpf@gmail.com", "Dan", 18,
                "Funcionario", "123123123", "123123", "123123");

        Funcionario funcionario2 =  new Funcionario(1, "Lucas@gmail.com", "Luc", 18,
                "Funcionario", "123123123", "123123", "123123");

        funcionarioService.cadastrarFuncionario(funcionario);
        funcionarioService.cadastrarFuncionario(funcionario2);

        assertEquals(2, funcionarioService.listarFuncionarios().size());
    }

    @Test
    public void testAtualizarFuncionario() {
        Funcionario funcionario =  new Funcionario(1, "dmelo@gmail.com", "Dan", 18,
                "Funcionario", "123123123", "123123", "123123");

        funcionarioService.cadastrarFuncionario(funcionario);

        Funcionario funcionarioAtualizado =  new Funcionario(1, "Lu@gmail.com", "Luc", 18,
                "Funcionario", "123123123", "123123", "123123");

        funcionarioService.atualizarFuncionario(1, funcionarioAtualizado);

        Funcionario retornoFuncionario = funcionarioService.encontrarFuncionarioPorId(1);
        assertEquals("Lu@gmail.com", retornoFuncionario.getEmail());
        assertEquals(18, retornoFuncionario.getIdade());
        assertEquals("Funcionario", retornoFuncionario.getFuncao());
        assertEquals("123123123", retornoFuncionario.getCpf());
        assertEquals("123123", retornoFuncionario.getSenha());
        assertEquals("123123", retornoFuncionario.getConfirmacaoSenha());
        assertEquals("Luc", retornoFuncionario.getNome());
    }

    @Test
    public void testExcluirFuncionario() {
        Funcionario funcionario =  new Funcionario(1, "dan@gmail.com", "Dan", 18,
                "Funcionario", "123123123", "123123", "123123");

        funcionarioService.cadastrarFuncionario(funcionario);

        assertEquals(1, funcionarioService.listarFuncionarios().size());

        funcionarioService.excluirFuncionario(1);

        assertEquals(0, funcionarioService.listarFuncionarios().size());
    }

    @Test
    public void testEncontrarFuncionarioPorIdDeveFalhar() {

        Funcionario funcionario = new Funcionario(1, "dan@gmail.com", "Daniel", 21, "Funcionario",
                "34234242", "34324234", "test");

        funcionarioService.cadastrarFuncionario(funcionario);

        assertThrows(FuncionarioNaoEncontradoException.class, () -> funcionarioService.encontrarFuncionarioPorId(2));
    }



}