package com.example.es2Daniel.service;

import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

import static org.junit.jupiter.api.Assertions.*;

@SpringBootTest
public class ExternoServiceTest {

    @Autowired
    private ExternoService externoService;

    @AfterEach
    public void clear(){
        externoService = null;
    }

    @Test
    public void testEnviarEmailDeveDarSucesso() {
        String resultado = externoService.enviarEmail();
        assertEquals("SUCESSO", resultado);
    }

}