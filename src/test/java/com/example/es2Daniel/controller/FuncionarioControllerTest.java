package com.example.es2Daniel.controller;

import com.example.es2Daniel.models.Funcionario;
import jakarta.validation.ConstraintViolationException;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.annotation.DirtiesContext;

import static org.junit.jupiter.api.Assertions.*;

@SpringBootTest
@DirtiesContext(classMode = DirtiesContext.ClassMode.AFTER_EACH_TEST_METHOD)
public class FuncionarioControllerTest {

    @Autowired
    private FuncionarioController funcionarioController;

    @AfterEach
    public void clear(){
        funcionarioController = null;
    }

    @Test
    public void testCadastrarFuncionarioCorpoInvalido(){
        Funcionario funcionario =  new Funcionario(1, null, "Dan", 18, "Funcionario", "123123123", "123123", "123123");

        assertThrows(ConstraintViolationException.class, () -> funcionarioController.cadastrarFuncionario(funcionario));
    }

    @Test
    public void testAtualizarFuncionarioCorpoInvalido(){
        Funcionario funcionario =  new Funcionario(1, null, "Dan", 18, "Funcionario", "123123123", "123123", "123123");

        assertThrows(ConstraintViolationException.class, () -> funcionarioController.atualizarFuncionario(1, funcionario));
    }

}