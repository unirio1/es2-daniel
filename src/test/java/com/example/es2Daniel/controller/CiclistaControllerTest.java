package com.example.es2Daniel.controller;

import com.example.es2Daniel.dto.requests.CadastroCiclista;
import com.example.es2Daniel.models.Cartao;
import com.example.es2Daniel.models.Ciclista;
import com.example.es2Daniel.models.Passaporte;
import jakarta.validation.ConstraintViolationException;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.annotation.DirtiesContext;

import static org.junit.jupiter.api.Assertions.*;

@SpringBootTest
@DirtiesContext(classMode = DirtiesContext.ClassMode.AFTER_EACH_TEST_METHOD)
public class CiclistaControllerTest {

    @Autowired
    private CiclistaController ciclistaController;

    @AfterEach
    public void clear(){
        ciclistaController = null;
    }

    @Test
    public void testCadastrarCiclistaSemCiclista(){
        Cartao cartao = new Cartao(1, "DPFM", "213213", "12/24", "213");

        CadastroCiclista cadastroCiclista = new CadastroCiclista(null, cartao);

        assertThrows(ConstraintViolationException.class, () -> ciclistaController.cadastrarCiclista(cadastroCiclista));
    }

    @Test
    public void testCadastrarCiclistaSemCartao(){
        Passaporte passaporte = new Passaporte("2198318937", "09/25", "Brasil");

        Ciclista ciclista = new Ciclista(1, "Ativo", "Daniel", "19/11/99", "3413213",
                passaporte, "Brasileiro", "dan@gmail.com", "test", "123");

        CadastroCiclista cadastroCiclista = new CadastroCiclista(ciclista, null);

        assertThrows(ConstraintViolationException.class, () -> ciclistaController.cadastrarCiclista(cadastroCiclista));
    }

    @Test
    public void testAtualizarCiclistaCorpoInvalido(){
        Passaporte passaporte = new Passaporte("2198318937", "09/25", "Brasil");

        Ciclista ciclista = new Ciclista(1, "Ativo", null, "19/11/99", "3413213",
                passaporte, "Brasileiro", "dan@gmail.com", "test", "123");

        assertThrows(ConstraintViolationException.class, () -> ciclistaController.atualizarCiclista(1, ciclista));
    }

    @Test
    public void testAtualizarCartaoCorpoInvalido(){
        Cartao cartao = new Cartao(1, "DPFM", null, "12/24", "213");

        assertThrows(ConstraintViolationException.class, () -> ciclistaController.atualizarMeioDePagamento(1, cartao));
    }

}