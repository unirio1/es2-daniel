package com.example.es2Daniel.enums;

import lombok.AllArgsConstructor;
import lombok.Getter;

@Getter
@AllArgsConstructor
public enum StatusCiclistaEnum {
    ATIVO("Ativo"),
    INATIVO("Inativo");

    private final String status;
}
