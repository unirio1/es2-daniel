package com.example.es2Daniel;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class Es2DanielApplication {

	public static void main(String[] args) {
		SpringApplication.run(Es2DanielApplication.class, args);
	}

}
