package com.example.es2Daniel.service;

import com.example.es2Daniel.exception.FuncionarioNaoEncontradoException;
import com.example.es2Daniel.models.Funcionario;
import lombok.NoArgsConstructor;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;

@Service
@NoArgsConstructor
public class FuncionarioService {

    private final List<Funcionario> listaFuncionarios = new ArrayList<>();
    private int idNovoFuncionario = 1;

    public Funcionario cadastrarFuncionario(Funcionario funcionario){
        funcionario.setMatricula(idNovoFuncionario);
        listaFuncionarios.add(funcionario);
        idNovoFuncionario++;
        return funcionario;
    }

    public List<Funcionario> listarFuncionarios(){ return listaFuncionarios; }

    public Funcionario atualizarFuncionario(Integer idFuncionario, Funcionario funcionarioAtualizado) {

        Funcionario funcionarioExistente = encontrarFuncionarioPorId(idFuncionario);

        funcionarioExistente.setEmail(funcionarioAtualizado.getEmail());
        funcionarioExistente.setNome(funcionarioAtualizado.getNome());
        funcionarioExistente.setIdade(funcionarioAtualizado.getIdade());
        funcionarioExistente.setFuncao(funcionarioAtualizado.getFuncao());
        funcionarioExistente.setCpf(funcionarioAtualizado.getCpf());
        funcionarioExistente.setSenha(funcionarioAtualizado.getSenha());
        funcionarioExistente.setConfirmacaoSenha(funcionarioAtualizado.getConfirmacaoSenha());

        return funcionarioExistente;
    }

    public void excluirFuncionario(Integer idFuncionario){
        Funcionario funcionarioExistente = encontrarFuncionarioPorId(idFuncionario);
        listaFuncionarios.remove(funcionarioExistente);
    }

    public Funcionario encontrarFuncionarioPorId(Integer idFuncionario) {
        for (Funcionario funcionario : listaFuncionarios){
            if (funcionario.getMatricula().equals(idFuncionario))
                return funcionario;
        }
        throw new FuncionarioNaoEncontradoException();
    }

    public void restaurarDados(){
        listaFuncionarios.clear();

        setIdNovoFuncinario(1);

        Funcionario funcionario = new Funcionario(12345, "employee@example.com", "Beltrano",
                25, "Beltrano", "99999999999", "123", "123");

        listaFuncionarios.add(funcionario);
    }

    public void setIdNovoFuncinario(int idNovoFuncionario) {
        this.idNovoFuncionario = idNovoFuncionario;
    }
}
