package com.example.es2Daniel.service;

import com.example.es2Daniel.dto.requests.AluguelRequest;
import com.example.es2Daniel.dto.requests.CadastroCiclista;
import com.example.es2Daniel.dto.requests.DevolucaoRequest;
import com.example.es2Daniel.dto.responses.AluguelResponse;
import com.example.es2Daniel.dto.responses.DevolucaoResponse;
import com.example.es2Daniel.exception.*;
import com.example.es2Daniel.models.*;
import lombok.AllArgsConstructor;
import lombok.NoArgsConstructor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.time.Duration;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;

import static com.example.es2Daniel.enums.StatusCiclistaEnum.ATIVO;
import static com.example.es2Daniel.enums.StatusCiclistaEnum.INATIVO;

@Service
@AllArgsConstructor
@NoArgsConstructor
public class CiclistaService {

    @Autowired
    private ExternoService externoService;

    @Autowired
    private EquipamentoService equipamentoService;

    private final List<Ciclista> listaCiclistas = new ArrayList<>();
    private final List<Cartao> listaCartoes = new ArrayList<>();
    private final List<CiclistaBicicleta> listaCiclistaComBicicletas = new ArrayList<>();
    private int idNovoCiclista = 1;

    private static final String SUCESSO = "SUCESSO";
    private static final Integer COBRANCA = 10;

    public String aloMundo(){
        return "Alo, mundo";
    }

    public Ciclista cadastrarCiclista(CadastroCiclista cadastroCiclista){

        if (existeEmailCiclista(cadastroCiclista.getCiclista().getEmail()))
            throw new EmailJaCadastradoException();

        cadastroCiclista.getCiclista().setId(idNovoCiclista);
        cadastroCiclista.getCartao().setIdCiclista(idNovoCiclista);
        cadastroCiclista.getCiclista().setStatus(INATIVO.getStatus());

        if (!externoService.enviarEmail().equals(SUCESSO)) {
            throw new ErroEnvioEmailException();
        }

        listaCiclistas.add(cadastroCiclista.getCiclista());
        listaCartoes.add(cadastroCiclista.getCartao());

        idNovoCiclista++;

        return cadastroCiclista.getCiclista();
    }

    public Ciclista encontrarCiclistaPorId(Integer idCiclista) {
        for (Ciclista ciclista : listaCiclistas){
            if (ciclista.getId().equals(idCiclista))
                return ciclista;
        }
        throw new CiclistaNaoEncontradoException();
    }

    public Ciclista atualizarCiclista(Integer idCiclista, Ciclista ciclistaAtualizado) {

        Ciclista ciclistaExistente = encontrarCiclistaPorId(idCiclista);

        ciclistaExistente.setEmail(ciclistaAtualizado.getEmail());
        ciclistaExistente.setNome(ciclistaAtualizado.getNome());
        ciclistaExistente.setNascimento(ciclistaAtualizado.getNascimento());
        ciclistaExistente.setNacionalidade(ciclistaAtualizado.getNacionalidade());
        ciclistaExistente.setCpf(ciclistaAtualizado.getCpf());
        ciclistaExistente.setSenha(ciclistaAtualizado.getSenha());
        ciclistaExistente.setPassaporte(ciclistaAtualizado.getPassaporte());
        ciclistaExistente.setUrlFotoDocumento(ciclistaAtualizado.getUrlFotoDocumento());

        if (!externoService.enviarEmail().equals(SUCESSO)) {
            throw new ErroEnvioEmailException();
        }

        return ciclistaExistente;
    }

    public Boolean existeEmailCiclista(String email){
        for (Ciclista ciclista : listaCiclistas){
            if (ciclista.getEmail().equals(email))
                return true;
        }
        return false;
    }

    public Ciclista ativarEmail(Integer idCiclista){

        Ciclista ciclista = encontrarCiclistaPorId(idCiclista);
        ciclista.setStatus(ATIVO.getStatus());
        return ciclista;
    }

    public Boolean permiteAluguel(Integer idCiclista) {
        Ciclista ciclista = encontrarCiclistaPorId(idCiclista);

        if (!ciclista.getStatus().equals(ATIVO.getStatus()))
            return false;

        for (CiclistaBicicleta ciclistaBicicleta : listaCiclistaComBicicletas){
            if (ciclistaBicicleta.getCiclista().getId().equals(idCiclista))
                return false;
        }
        return true;
    }

    public Bicicleta encontraBicicletaAlugadaPorIdCiclista(Integer idCiclista) {
        encontrarCiclistaPorId(idCiclista);

        for (CiclistaBicicleta ciclistaBicicleta : listaCiclistaComBicicletas){
            if (ciclistaBicicleta.getCiclista().getId().equals(idCiclista))
                return ciclistaBicicleta.getBicicleta();
        }
        return null;
    }

    public Cartao encontrarMeioDePagamento(Integer idCiclista) {
        for (Cartao cartao : listaCartoes){
            if (cartao.getIdCiclista().equals(idCiclista))
                return cartao;
        }
        throw new CartaoNaoEncontradoException();
    }

    public Cartao atualizarMeioDePagamento(Integer idCiclista, Cartao cartaoAtualizado) {

        Cartao cartaoExistente = encontrarMeioDePagamento(idCiclista);
        cartaoExistente.setCvv(cartaoAtualizado.getCvv());
        cartaoExistente.setNumero(cartaoAtualizado.getNumero());
        cartaoExistente.setNomeTitular(cartaoAtualizado.getNomeTitular());
        cartaoExistente.setValidade(cartaoAtualizado.getValidade());

        return cartaoExistente;
    }

    public AluguelResponse realizarAluguel(AluguelRequest aluguelRequest) {
        Ciclista ciclista = encontrarCiclistaPorId(aluguelRequest.getCiclista());

        if (!permiteAluguel(aluguelRequest.getCiclista()))
            throw new AluguelNaoPermitidoException();

        if (!externoService.enviarCobranca().equals(SUCESSO))
            throw new ErroEnviarCobrancaException();

        Bicicleta bicicleta = new Bicicleta(1, "MarcaBike", "Teste", "2022", 131, "Em uso");

        CiclistaBicicleta ciclistaBicicleta = new CiclistaBicicleta(ciclista, bicicleta, LocalDateTime.now(), LocalDateTime.now().plusHours(2));

        listaCiclistaComBicicletas.add(ciclistaBicicleta);

        if (!equipamentoService.alteraStatusTranca().equals(SUCESSO))
            throw new ErroAlterarStatusTrancaException();

        if (!externoService.enviarEmail().equals(SUCESSO)) {
            throw new ErroEnvioEmailException();
        }

        return new AluguelResponse(ciclistaBicicleta.getBicicleta().getIdBicicleta(), ciclistaBicicleta.getHoraInicio().toString(), 34,
                ciclistaBicicleta.getHoraFim().toString(), COBRANCA, aluguelRequest.getCiclista(), aluguelRequest.getTrancaInicio());
    }

    public DevolucaoResponse realizarDevolucao(DevolucaoRequest devolucaoRequest) {

        if (!equipamentoService.validaNumeroBicicleta().equals(SUCESSO))
            throw new NumeroBicicletaInvalidoException();

        CiclistaBicicleta ciclistaBicicleta = encontraCiclistaBicicletaPorIdBicicleta(devolucaoRequest.getIdBicicleta());
        ciclistaBicicleta.setHoraDevolucao(LocalDateTime.now());

        long duracaoMinutos = Duration.between(ciclistaBicicleta.getHoraInicio(), ciclistaBicicleta.getHoraDevolucao()).toMinutes();
        double horas = (double) duracaoMinutos / 60;
        long minutosExtras = duracaoMinutos - 120;

        long cobrancaExtra = 0;

        if (horas > 2)
            cobrancaExtra = (Math.floorDiv(minutosExtras, 30) * 5);

        externoService.enviarCobranca();
        equipamentoService.alteraStatusTranca();
        listaCiclistaComBicicletas.remove(ciclistaBicicleta);

        return new DevolucaoResponse(ciclistaBicicleta.getBicicleta().getIdBicicleta(), ciclistaBicicleta.getHoraInicio().toString(), devolucaoRequest.getIdTranca(),
                ciclistaBicicleta.getHoraDevolucao().toString(), cobrancaExtra, ciclistaBicicleta.getCiclista().getId());
    }

    public CiclistaBicicleta encontraCiclistaBicicletaPorIdBicicleta(Integer idBicicleta) {
        for (CiclistaBicicleta ciclistaBicicleta : listaCiclistaComBicicletas){
            if (ciclistaBicicleta.getBicicleta().getIdBicicleta() == idBicicleta)
                return ciclistaBicicleta;
        }
        return null;
    }

    public void restaurarDados(){
        listaCartoes.clear();
        listaCiclistas.clear();
        listaCiclistaComBicicletas.clear();

        setIdNovoCiclista(1);

        Passaporte passaporte1 = new Passaporte( "2198318937" ,"09/25", "Brasil");

        Cartao cartao1 = new Cartao(idNovoCiclista, "Fulano Beltrano", "4012001037141112", "2022-12", "132");

        Ciclista ciclista1 = new Ciclista(idNovoCiclista, "Ativo", "Fulano Beltrano", "2021-05-02", "78804034009", passaporte1, "Brasileiro",
                "user@example.com", "foto.png", "ABC123");

        CadastroCiclista cadastroCiclista1 = new CadastroCiclista(ciclista1, cartao1);

        cadastrarCiclista(cadastroCiclista1);

        //////

        Passaporte passaporte2 = new Passaporte( "2198318939" ,"08/26", "Brasil");

        Cartao cartao2 = new Cartao(idNovoCiclista, "Fulano Beltrano", "4012001037141112", "2022-12", "132");

        Ciclista ciclista2 = new Ciclista(idNovoCiclista, "Inativo", "Fulano Beltrano", "2021-05-02", "43943488039",
                passaporte2, "Brasileiro", "user2@example.com", "foto.png", "ABC123");

        CadastroCiclista cadastroCiclista2 = new CadastroCiclista(ciclista2, cartao2);

        cadastrarCiclista(cadastroCiclista2);

        //////

        Passaporte passaporte3 = new Passaporte( "2198318339" ,"01/29", "Brasil");

        Cartao cartao3 = new Cartao(idNovoCiclista, "Fulano Beltrano", "4012001037141112", "2022-12", "132");

        Ciclista ciclista3 = new Ciclista(idNovoCiclista, "Ativo", "Fulano Beltrano", "2021-05-02", "10243164084",
                passaporte3, "Brasileiro", "user3@example.com", "foto.png", "ABC123");

        CadastroCiclista cadastroCiclista3 = new CadastroCiclista(ciclista3, cartao3);

        cadastrarCiclista(cadastroCiclista3);

        //////

        Passaporte passaporte4 = new Passaporte( "2198318936" ,"03/26", "Brasil");

        Cartao cartao4 = new Cartao(idNovoCiclista, "Fulano Beltrano", "4012001037141112", "2022-12", "132");

        Ciclista ciclista4 = new Ciclista(idNovoCiclista, "Ativo", "Fulano Beltrano", "2021-05-02", "30880150017",
                passaporte4, "Brasileiro", "user4@example.com", "foto.png", "ABC123");

        CadastroCiclista cadastroCiclista4 = new CadastroCiclista(ciclista4, cartao4);

        cadastrarCiclista(cadastroCiclista4);
    }

    public void setIdNovoCiclista(int idNovoCiclista) {
        this.idNovoCiclista = idNovoCiclista;
    }
}
