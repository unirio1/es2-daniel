package com.example.es2Daniel.service;

import lombok.NoArgsConstructor;
import org.springframework.stereotype.Service;

@Service
@NoArgsConstructor
public class ExternoService {

    public String enviarEmail(){
        return "SUCESSO";
    }

    public String enviarCobranca(){
        return "SUCESSO";
    }
}
