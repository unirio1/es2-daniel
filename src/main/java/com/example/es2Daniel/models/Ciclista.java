package com.example.es2Daniel.models;

import jakarta.validation.constraints.NotNull;
import lombok.*;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
public class Ciclista {

    private Integer id;

    private String status;

    @NotNull(message = "O atributo 'nome' não pode ser nulo")
    private String nome;

    @NotNull(message = "O atributo 'nascimento' não pode ser nulo")
    private String nascimento;

    @NotNull(message = "O atributo 'cpf' não pode ser nulo")
    private String cpf;

    @NotNull(message = "O atributo 'passaporte' não pode ser nulo")
    private Passaporte passaporte;

    @NotNull(message = "O atributo 'nacionalidade' não pode ser nulo")
    private String nacionalidade;

    @NotNull(message = "O atributo 'email' não pode ser nulo")
    private String email;

    @NotNull(message = "O atributo 'urlFotoDocumento' não pode ser nulo")
    private String urlFotoDocumento;

    @NotNull(message = "O atributo 'senha' não pode ser nulo")
    private String senha;
}
