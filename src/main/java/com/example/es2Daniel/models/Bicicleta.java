package com.example.es2Daniel.models;

import jakarta.validation.constraints.NotNull;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
public class Bicicleta {

    @NotNull(message = "O atributo 'idBicicleta' não pode ser nulo")
    private int idBicicleta;

    @NotNull(message = "O atributo 'marca' não pode ser nulo")
    private String marca;

    @NotNull(message = "O atributo 'modelo' não pode ser nulo")
    private String modelo;

    @NotNull(message = "O atributo 'ano' não pode ser nulo")
    private String ano;

    @NotNull(message = "O atributo 'numero' não pode ser nulo")
    private int numero;

    @NotNull(message = "O atributo 'status' não pode ser nulo")
    private String status;

}
