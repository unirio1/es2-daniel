package com.example.es2Daniel.models;

import jakarta.validation.constraints.NotNull;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
public class Cartao {

    private Integer idCiclista;

    @NotNull(message = "O atributo 'nomeTitular' não pode ser nulo")
    private String nomeTitular;

    @NotNull(message = "O atributo 'numero' não pode ser nulo")
    private String numero;

    @NotNull(message = "O atributo 'validade' não pode ser nulo")
    private String validade;

    @NotNull(message = "O atributo 'cvv' não pode ser nulo")
    private String cvv;

}
