package com.example.es2Daniel.controller;

import com.example.es2Daniel.dto.responses.RespostaErro;
import com.example.es2Daniel.exception.*;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;

@ControllerAdvice
public class ExceptionController {

    @ExceptionHandler(CiclistaNaoEncontradoException.class)
    public ResponseEntity<RespostaErro> handleCiclistaNaoEncontradoException(CiclistaNaoEncontradoException ex) {
        RespostaErro respostaErro = new RespostaErro(HttpStatus.NOT_FOUND.value(), ex.getMessage());
        return new ResponseEntity<>(respostaErro, HttpStatus.NOT_FOUND);
    }

    @ExceptionHandler(CartaoNaoEncontradoException.class)
    public ResponseEntity<RespostaErro> handleCartaoNaoEncontradoException(CartaoNaoEncontradoException ex) {
        RespostaErro respostaErro = new RespostaErro(HttpStatus.NOT_FOUND.value(), ex.getMessage());
        return new ResponseEntity<>(respostaErro, HttpStatus.NOT_FOUND);
    }

    @ExceptionHandler(FuncionarioNaoEncontradoException.class)
    public ResponseEntity<RespostaErro> handleFuncionarioNaoEncontradoException(FuncionarioNaoEncontradoException ex) {
        RespostaErro respostaErro = new RespostaErro(HttpStatus.NOT_FOUND.value(), ex.getMessage());
        return new ResponseEntity<>(respostaErro, HttpStatus.NOT_FOUND);
    }

    @ExceptionHandler(ErroEnvioEmailException.class)
    public ResponseEntity<RespostaErro> handleErroEnvioEmailException(ErroEnvioEmailException ex) {
        RespostaErro respostaErro = new RespostaErro(HttpStatus.CONFLICT.value(), ex.getMessage());
        return new ResponseEntity<>(respostaErro, HttpStatus.CONFLICT);
    }

    @ExceptionHandler(EmailJaCadastradoException.class)
    public ResponseEntity<RespostaErro> handleEmailJaCadastradoException(EmailJaCadastradoException ex) {
        RespostaErro respostaErro = new RespostaErro(HttpStatus.CONFLICT.value(), ex.getMessage());
        return new ResponseEntity<>(respostaErro, HttpStatus.CONFLICT);
    }

    @ExceptionHandler(ErroEnviarCobrancaException.class)
    public ResponseEntity<RespostaErro> handleErroEnviarCobrancaException(ErroEnviarCobrancaException ex) {
        RespostaErro respostaErro = new RespostaErro(HttpStatus.CONFLICT.value(), ex.getMessage());
        return new ResponseEntity<>(respostaErro, HttpStatus.CONFLICT);
    }

    @ExceptionHandler(AluguelNaoPermitidoException.class)
    public ResponseEntity<RespostaErro> handleAluguelNaoPermitidoException(AluguelNaoPermitidoException ex) {
        RespostaErro respostaErro = new RespostaErro(HttpStatus.CONFLICT.value(), ex.getMessage());
        return new ResponseEntity<>(respostaErro, HttpStatus.CONFLICT);
    }

    @ExceptionHandler(ErroAlterarStatusTrancaException.class)
    public ResponseEntity<RespostaErro> handleErroAlterarStatusTrancaException(ErroAlterarStatusTrancaException ex) {
        RespostaErro respostaErro = new RespostaErro(HttpStatus.CONFLICT.value(), ex.getMessage());
        return new ResponseEntity<>(respostaErro, HttpStatus.CONFLICT);
    }

    @ExceptionHandler(NumeroBicicletaInvalidoException.class)
    public ResponseEntity<RespostaErro> handleNumeroBicicletaInvalido(NumeroBicicletaInvalidoException ex) {
        RespostaErro respostaErro = new RespostaErro(HttpStatus.CONFLICT.value(), ex.getMessage());
        return new ResponseEntity<>(respostaErro, HttpStatus.CONFLICT);
    }
}
