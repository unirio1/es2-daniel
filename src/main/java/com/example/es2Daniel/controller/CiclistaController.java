package com.example.es2Daniel.controller;

import com.example.es2Daniel.dto.requests.AluguelRequest;
import com.example.es2Daniel.dto.requests.CadastroCiclista;
import com.example.es2Daniel.dto.requests.DevolucaoRequest;
import com.example.es2Daniel.dto.responses.AluguelResponse;
import com.example.es2Daniel.dto.responses.DevolucaoResponse;
import com.example.es2Daniel.models.Bicicleta;
import com.example.es2Daniel.models.Cartao;
import com.example.es2Daniel.models.Ciclista;;
import com.example.es2Daniel.service.CiclistaService;
import jakarta.validation.Valid;
import lombok.AllArgsConstructor;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

@RestController
@AllArgsConstructor
@Validated
public class CiclistaController {

    private final CiclistaService ciclistaService;

    @GetMapping("/alo-mundo")
    public String getAloMundo () {
        return ciclistaService.aloMundo();
    }

    @PostMapping("/ciclista")
    public Ciclista cadastrarCiclista(@Valid @RequestBody CadastroCiclista cadastroCiclista) {
        return ciclistaService.cadastrarCiclista(cadastroCiclista);
    }

    @GetMapping("/ciclista/{idCiclista}")
    public Ciclista encontrarCiclistaPorID(@PathVariable Integer idCiclista) {
        return ciclistaService.encontrarCiclistaPorId(idCiclista);
    }

    @PutMapping("/ciclista/{idCiclista}")
    public Ciclista atualizarCiclista(@PathVariable Integer idCiclista, @Valid @RequestBody Ciclista ciclistaAtualizado) {
        return ciclistaService.atualizarCiclista(idCiclista, ciclistaAtualizado);
    }

    @GetMapping("/ciclista/existeEmail/{email}")
    public Boolean existeEmailCiclista(@PathVariable String email){
        return ciclistaService.existeEmailCiclista(email);
    }

    @PostMapping("/ciclista/{idCiclista}/ativar")
    public Ciclista ativarEmail(@PathVariable Integer idCiclista){
        return ciclistaService.ativarEmail(idCiclista);
    }

    @GetMapping("/ciclista/{idCiclista}/permiteAluguel")
    public Boolean permiteAluguel(@PathVariable Integer idCiclista){
        return ciclistaService.permiteAluguel(idCiclista);
    }

    @GetMapping("/ciclista/{idCiclista}/bicicletaAlugada")
    public Bicicleta bicicletaAlugada(@PathVariable Integer idCiclista){
        return ciclistaService.encontraBicicletaAlugadaPorIdCiclista(idCiclista);
    }

    @GetMapping("/cartaoDeCredito/{idCiclista}")
    public Cartao encontrarMeioDePagamento(@PathVariable Integer idCiclista){
        return ciclistaService.encontrarMeioDePagamento(idCiclista);
    }

    @PutMapping("/cartaoDeCredito/{idCiclista}")
    public Cartao atualizarMeioDePagamento(@PathVariable Integer idCiclista, @Valid @RequestBody Cartao cartaoAtualizado){
        return ciclistaService.atualizarMeioDePagamento(idCiclista, cartaoAtualizado);
    }

    @PostMapping("/aluguel")
    public AluguelResponse realizarAluguel(@Valid @RequestBody AluguelRequest aluguelRequest){
        return ciclistaService.realizarAluguel(aluguelRequest);
    }

    @PostMapping("/devolucao")
    public DevolucaoResponse realizarDevolucao(@Valid @RequestBody DevolucaoRequest devolucaoRequest){
        return ciclistaService.realizarDevolucao(devolucaoRequest);
    }

    @PostMapping("/ciclista/restaurarDados")
    public void restaurarDados(){
        ciclistaService.restaurarDados();
    }
}
