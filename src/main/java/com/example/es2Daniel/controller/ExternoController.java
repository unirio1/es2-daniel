package com.example.es2Daniel.controller;

import com.example.es2Daniel.service.ExternoService;
import lombok.AllArgsConstructor;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@AllArgsConstructor
@Validated
public class ExternoController {

    private final ExternoService externoService;

    @PostMapping("/enviarEmail")
    public String enviarEmail() {
        return externoService.enviarEmail();
    }
}
