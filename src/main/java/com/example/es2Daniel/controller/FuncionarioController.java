package com.example.es2Daniel.controller;

import com.example.es2Daniel.models.Funcionario;
import com.example.es2Daniel.service.FuncionarioService;
import jakarta.validation.Valid;
import lombok.AllArgsConstructor;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@AllArgsConstructor
@Validated
public class FuncionarioController {

    private final FuncionarioService funcionarioService;

    @PostMapping("/funcionario")
    public Funcionario cadastrarFuncionario(@Valid @RequestBody Funcionario funcionario) {
        return funcionarioService.cadastrarFuncionario(funcionario);
    }

    @GetMapping("/funcionario")
    public List<Funcionario> listarFuncionarios() {
        return funcionarioService.listarFuncionarios();
    }

    @GetMapping("/funcionario/{idFuncionario}")
    public Funcionario encontrarFuncionarioPorId(@PathVariable Integer idFuncionario) {
        return funcionarioService.encontrarFuncionarioPorId(idFuncionario);
    }

    @PutMapping("/funcionario/{idFuncionario}")
    public Funcionario atualizarFuncionario(@PathVariable Integer idFuncionario, @Valid @RequestBody Funcionario funcionarioAtualizado) {
        return funcionarioService.atualizarFuncionario(idFuncionario, funcionarioAtualizado);
    }

    @DeleteMapping("/funcionario/{idFuncionario}")
    public void excluirFuncionario(@PathVariable Integer idFuncionario){
        funcionarioService.excluirFuncionario(idFuncionario);
    }

    @PostMapping("/funcionario/restaurarDados")
    public void restaurarDados(){
        funcionarioService.restaurarDados();
    }
}
