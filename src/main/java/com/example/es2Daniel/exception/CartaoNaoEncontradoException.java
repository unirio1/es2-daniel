package com.example.es2Daniel.exception;

public class CartaoNaoEncontradoException extends RuntimeException{
    public CartaoNaoEncontradoException() {
        super("Erro: Cartao não encontrado!");
    }
}
