package com.example.es2Daniel.exception;

public class ErroEnviarCobrancaException extends RuntimeException{
    public ErroEnviarCobrancaException() {
        super("Erro: Erro no envio de cobranca");
    }
}
