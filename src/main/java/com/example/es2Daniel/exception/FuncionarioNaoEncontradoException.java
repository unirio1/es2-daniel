package com.example.es2Daniel.exception;

public class FuncionarioNaoEncontradoException extends RuntimeException{
    public FuncionarioNaoEncontradoException() {
        super("Erro: Funcionario não encontrado!");
    }
}
