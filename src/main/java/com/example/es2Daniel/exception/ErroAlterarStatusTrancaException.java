package com.example.es2Daniel.exception;

public class ErroAlterarStatusTrancaException extends RuntimeException{
    public ErroAlterarStatusTrancaException() {super("Erro: Erro ao alterar status da tranca.");}
}
