package com.example.es2Daniel.exception;

public class NumeroBicicletaInvalidoException extends RuntimeException{
    public NumeroBicicletaInvalidoException() {
        super("Erro: Numero de bicicleta invalido");
    }
}
