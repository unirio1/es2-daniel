package com.example.es2Daniel.exception;

public class ErroEnvioEmailException extends RuntimeException{
    public ErroEnvioEmailException() {
        super("Erro: Erro no envio de Email");
    }
}
