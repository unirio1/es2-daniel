package com.example.es2Daniel.exception;

public class EmailJaCadastradoException extends RuntimeException{
    public EmailJaCadastradoException() {
        super("Erro: Email ja cadastrado");
    }
}
