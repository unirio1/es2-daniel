package com.example.es2Daniel.exception;

public class AluguelNaoPermitidoException extends RuntimeException{
    public AluguelNaoPermitidoException() {super("Erro: Aluguel não permitido.");}
}
