package com.example.es2Daniel.exception;

public class CiclistaNaoEncontradoException extends RuntimeException {
    public CiclistaNaoEncontradoException() {
        super("Erro: Ciclista não encontrado!");
    }
}
