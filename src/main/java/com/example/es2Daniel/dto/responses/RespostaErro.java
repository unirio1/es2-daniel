package com.example.es2Daniel.dto.responses;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
@AllArgsConstructor
public class RespostaErro {
    private int status;
    private String mensagem;
}
