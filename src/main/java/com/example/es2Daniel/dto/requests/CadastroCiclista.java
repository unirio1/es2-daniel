package com.example.es2Daniel.dto.requests;


import com.example.es2Daniel.models.Ciclista;
import com.example.es2Daniel.models.Cartao;
import jakarta.validation.constraints.NotNull;
import lombok.*;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
public class CadastroCiclista {

    @NotNull(message = "O atributo 'ciclista' não pode ser nulo")
    private Ciclista ciclista;

    @NotNull(message = "O atributo 'cartao' não pode ser nulo")
    private Cartao cartao;
}
