package com.example.es2Daniel.dto.requests;

import jakarta.validation.constraints.NotNull;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
public class DevolucaoRequest {
    @NotNull(message = "O atributo 'idTranca' não pode ser nulo")
    private Integer idTranca;

    @NotNull(message = "O atributo 'idBicicleta' não pode ser nulo")
    private Integer idBicicleta;
}
