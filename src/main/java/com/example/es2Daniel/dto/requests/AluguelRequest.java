package com.example.es2Daniel.dto.requests;

import jakarta.validation.constraints.NotNull;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
public class AluguelRequest {
    @NotNull(message = "O atributo 'ciclista' não pode ser nulo")
    private Integer ciclista;

    @NotNull(message = "O atributo 'trancaInicio' não pode ser nulo")
    private Integer trancaInicio;
}
