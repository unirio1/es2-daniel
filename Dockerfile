FROM maven:3.9.0-eclipse-temurin-17-alpine AS build
COPY . .
RUN mvn clean package

FROM openjdk:17-jdk-alpine
COPY --from=build /target/es2Daniel-0.0.1-SNAPSHOT.jar es2Daniel-0.0.1-SNAPSHOT.jar


EXPOSE 8080
ENTRYPOINT ["java","-jar","es2Daniel-0.0.1-SNAPSHOT.jar"]
